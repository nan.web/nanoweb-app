import path from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const DIST_URI = 'build';
const PUBLIC_URI = 'public';

export default {
	// httpPort: 10080,
	httpsPort: 10443,
	DIST_URI,
	DIST_DIR: path.join(__dirname, DIST_URI),
	PUBLIC_URI,
	PUBLIC_DIR: path.join(__dirname, PUBLIC_URI),
	sslEnabled: false,
	github: {
		clientId: 'Ov23ctxUa787EzBGAY8A',
		redirectUri: 'https://nanoweb.yaro.page/auth.php?state=github',
		authUrl: 'https://github.com/login/oauth/authorize',
	},
	gitlab: {
		clientId: '9c550e41b3ebffa04589f9ae0debbcbde4e6c8e7f79480150e0de218cd16671b',
		redirectUri: 'https://nanoweb.yaro.page/auth.php?state=gitlab',
		authUrl: 'https://gitlab.com/oauth/authorize',
	},
	sslKey: './keys/server.key',
	sslCert: './keys/server.cert',
};
