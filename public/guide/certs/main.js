document.querySelectorAll('[href="#server.cert"]').forEach($a => {
	$a.addEventListener('click', async function (event) {
		event.preventDefault();
		try {
			await window.api.openCertsLocation();
			console.log('File explorer opened successfully');
		} catch (err) {
			console.error('Failed to open certificate file location:', err);
		}
	})
});
document.querySelectorAll('loc[file]').forEach(async ($loc) => {
	const dir = await window.api.getCurrentDirectory();
	$loc.innerHTML = dir + '/' + $loc.getAttribute('file');
});

/**
 * Hide all the [on-platform] that do not match the current one.
 * Possible values ['mac', 'macos', 'win', 'windows', 'lin', 'linux'].
 * @example <div on-platform="mac">
 * @example <div on-platform="win,lin">
 */
(async () => {
	function getPlatform() {
		const userAgent = window.navigator.userAgent;
		if (userAgent.indexOf('Win') !== -1) return ['win', 'windows'];
		if (userAgent.indexOf('Mac') !== -1) return ['mac', 'macos'];
		if (userAgent.indexOf('Linux') !== -1) return ['lin', 'linux'];
		return null;
	}
	
	function showPlatformOnly() {
		const platform = getPlatform();
		console.log(`Detected platform`, platform);
		if (null === platform) return;
		document.querySelectorAll('[on-platform]').forEach($el => {
			const target = ($el.getAttribute('on-platform') || '').split(',');
			const on = target.some(value => platform.includes(value));
			if (on) {
				// keep visible.
			} else {
				$el.style.display = 'none';
			}
		});
	}
	
	document.addEventListener('DOMContentLoaded', showPlatformOnly);
})();