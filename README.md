# Nan•web application
— build with Electron

## Features
- Intuitive content management
- Real-time collaboration
- Flexible deployment options

## Installation

```sh
git clone https://gitlab.com/nan.web/nanoweb-app.git
cd nanoweb-app
npm install
```

### Usage

```sh
npm start
```

### Building and Packaging

```sh
npm run dist
```
