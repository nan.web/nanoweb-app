const { contextBridge, ipcRenderer } = require('electron');

window.addEventListener('error', (event) => {
	const fs = require('fs');
	const path = require('path');
	
	const logPath = path.join(require('electron').remote.app.getPath('userData'), 'renderer.log');
	fs.appendFileSync(logPath, `Preload.js Error: ${event.message}\n`);
	console.error(event.message);
});

window.addEventListener('DOMContentLoaded', () => {
	console.log('Preload script executed');
});

contextBridge.exposeInMainWorld('versions', {
	node: () => process.versions.node,
	chrome: () => process.versions.chrome,
	electron: () => process.versions.electron
});
// Expose a safe API to the renderer process
contextBridge.exposeInMainWorld('api', {
	invoke: (channel, ...args) => ipcRenderer.invoke(channel, ...args),
	send: (channel, data) => {
		// List of allowed channels
		let validChannels = ['toMain'];
		if (validChannels.includes(channel)) {
			ipcRenderer.send(channel, data);
		}
	},
	receive: (channel, func) => {
		let validChannels = ['fromMain'];
		if (validChannels.includes(channel)) {
			// Remove any existing listeners for this channel to avoid potential issues with multiple listeners
			ipcRenderer.removeAllListeners(channel);
			// Deliberately strip event as it includes `sender`
			ipcRenderer.on(channel, (event, ...args) => func(...args));
		}
	},
	saveConfig: async (config) => {
		return ipcRenderer.send('config:save', config);
	},
	getConfig: () => ipcRenderer.invoke('config:get'),
	selectDirectory: async (dir) => {
		return ipcRenderer.invoke('select-directory', dir);
	},
	getCurrentDirectory: () => ipcRenderer.invoke('get-current-directory'),
	openCertsLocation: () => ipcRenderer.invoke('open-cert-file-location'),
	openExternal: (url) => {
		ipcRenderer.send('open-external', url);
	},

	// GIT
	readGit: ({ website, dir, index }) => {
		ipcRenderer.send('git:read', { website, dir, index });
	},
	onGitReadSuccess: (callback) => ipcRenderer.on('git:read:on:success', (_event, value) => callback(value)),
	onGitReadFail: (callback) => ipcRenderer.on('git:read:on:fail', (_event, value) => callback(value)),
	removeGitReadResponse: () => ipcRenderer.off('git:read:on:success', () => {}),
	removeGitReadFail: () => ipcRenderer.off('git:read:on:fail', () => {}),
	gitInit: (website, dir) => {
		ipcRenderer.send('get-init', website, dir);
	},
	gitConnect2Github: (website, dir) => {
		ipcRenderer.send('oauth-login', 'github');
		// ipcRenderer.send('git-connect2github', website, dir);
	},
	gitConnect2Gitlab: (website, dir) => {
		ipcRenderer.send('oauth-login', 'gitlab');
		// ipcRenderer.send('git-connect2gitlab', website, dir);
	},
	gitSaveUrl: (website, dir, url) => {
		ipcRenderer.send('git-save-url', website, dir, url);
	},
	gitOnReposList: (callback) => ipcRenderer.on('repos-list', (event, repos) => callback(repos)),

	// WEBSITES
	startWebsite: (target) => {
		ipcRenderer.invoke('website:start', target);
	},
	showWebsite: (website) => {
		ipcRenderer.invoke('website:show', website);
	},
	hideWebsite: () => {
		ipcRenderer.invoke('website:hide');
	},
	removeWebsite: (target) => {
		ipcRenderer.invoke('website:remove', target);
	},
	onWebsiteStartSuccess: (callback) => ipcRenderer.on('website:start:on:success', (_event, value) => callback(value)),
	onWebsiteStartFail: (callback) => ipcRenderer.on('website:start:on:fail', (_event, value) => callback(value)),
	onWebsiteStartLog: (callback) => ipcRenderer.on('website:start:on:log', (_event, value) => callback(value)),
	onWebsiteHide: (callback) => ipcRenderer.on('website:on:hide', (_event, value) => callback(value)),
	onWebsiteSelect: (callback) => ipcRenderer.on('website:on:select', (_event, value) => callback(value)),
	removeWebsiteStartSuccess: () => ipcRenderer.off('website:start:on:success', () => {}), 
	removeWebsiteStartFail: () => ipcRenderer.off('website:start:on:fail', () => {}), 
	removeWebsiteStartLog: () => ipcRenderer.off('website:start:on:log', () => {}),

	// APP
	quit: () => {
		ipcRenderer.invoke('quit');
	},
});
