import React, { useEffect, useState } from 'react';
import { LanguageProvider, useLanguageContext } from './context/LanguageContext';
import { Helmet } from "react-helmet";
import LanguageSelector from './components/LanguageSelector';
import Websites from './components/Websites';
import Loading from './components/Loading';
import ErrorPopup from './components/ErrorPopup';
import PanelNav from './components/PanelNav';
import Publisher from './components/Publisher';
import { ErrorBoundary } from "react-error-boundary";
import { setupExternalLinkHandler } from './context/Url';

function App({ serverLog, serverStatus }) {
	const { locale, loading, setLoading, config, language: _, setConfig, setActiveWebsite, page } = useLanguageContext();
	const [localConfig, setLocalConfig] = useState(null);

	useEffect(() => {
		setupExternalLinkHandler();
	}, []);

	useEffect(() => {
		const handleSuccess = ({ win, url, website, dir, index }) => {
			console.log(win, url, website, dir, index);
			setLoading(false);
		};
		window.api.onWebsiteStartSuccess(handleSuccess);

		return () => {
			window.api.removeWebsiteStartSuccess(handleSuccess);
		};
	}, []);

	async function fetchConfig() {
		try {
			const fetchedConfig = await window.api.getConfig();
			setLocalConfig(fetchedConfig);
			console.log('getConfig', fetchedConfig, localConfig);
		} catch (error) {
			console.error("Failed to load config:", error);
		}
	}

	useEffect(() => {
		fetchConfig();
	}, []);

	useEffect(() => {
		// Keep the local config in sync with the context config
		if (config) {
			setLocalConfig(config);
		}
	}, [config]);

	const onWebsiteSelect = ({ website, dir, index }) => {
		const updatedConfig = setActiveWebsite(index);
		setLocalConfig(updatedConfig);
	};

	const onWebsiteSave = (updatedWebsites) => {
		const updatedConfig = {
			...localConfig,
			websites: updatedWebsites
		};
		setConfig(updatedConfig); // Save to context and main process
		fetchConfig();
		// setLocalConfig(updatedConfig);
	};

	const onWebsiteRemove = ({ website, updatedWebsites, dir, index }) => {
		onWebsiteSave(updatedWebsites);
		window.api.removeWebsite({ website, dir, index });
	};

	if (loading) {
		return (
			<main className='wizard'><Loading /></main>
		);
	}

	if (!locale || 'languages' === page) return <main className='wizard'><LanguageSelector /></main>;

	let className = 'websites';
	if ('publish' === page) className = 'publish';
	return (
		<>
			<main className={className}>
				{localConfig && ['', 'websites'].includes(page) && (
					<Websites
						websites={localConfig.websites}
						dirs={localConfig.dirs}
						activeWebsite={localConfig.activeWebsite}
						serverStatus={serverStatus}
						onSelect={onWebsiteSelect}
						onSave={onWebsiteSave}
						onRemove={onWebsiteRemove}
					/>
				)}
				{localConfig && ['publish'].includes(page) && (
					<Publisher />
				)}
				{serverLog.length > 0 && (
					<div className="list-group container server-log">
						{serverLog.map((log, index) => (
							<div key={index} className='list-group-item'>
								<p className={`badge ${log.type === 'stderr' ? 'bg-danger' : 'bg-secondary'}`}>{log.website.ROOT_DIR}</p>
								<pre>{log.data.trim()}</pre>
							</div>
						))}
					</div>
				)}
			</main>
		</>
	);
}

function WrappedApp() {
	const { setMode } = useLanguageContext();
	const [serverLog, setServerLog] = useState([]);
	const [serverError, setServerError] = useState(null);
	const [serverStatus, setServerStatus] = useState({});
	useEffect(() => {
		const handleError = ({ error, website, dir, index }) => {
			if (/EADDRINUSE/.test(error) && (new RegExp(`:::${website.SERVER_PORT}`)).test(error)) {
				setServerError(_['EADDRINUSE'] || 'Website port already in use');
				setServerStatus(prev => ({ ...prev, [website.ROOT_DIR]: 'used' }));
				return;
			}
			setServerError('' + error);
		};
		window.api.onWebsiteStartFail(handleError);

		return () => {
			window.api.removeWebsiteStartFail(handleError);
		};
	}, []);

	useEffect(() => {
		const handleLog = ({ type, data, website }) => {
			setServerLog(prevLogs => [...prevLogs, { type, data, website }]);
			const text = data.trim();
			// if (text.startsWith('Server started at') || text.includes('Loaded all the data files')) {
			if (text.startsWith('Server started at')) {
				setServerStatus(prev => ({ ...prev, [website.ROOT_DIR]: 'started' }));
			}
			if (text.includes('EADDRINUSE') && type === 'stderr') {
				setServerStatus(prev => ({ ...prev, [website.ROOT_DIR]: 'used' }));
			}
		};
		window.api.onWebsiteStartLog(handleLog);

		return () => {
			window.api.removeWebsiteStartLog(handleLog);
		};
	}, []);

	useEffect(() => {
		window.api.onWebsiteHide(() => { setMode('app') });
		window.api.onWebsiteSelect(() => { setMode('nav') });
	}, []);

	const handleErrorClose = () => {
		setServerError(null);
	};

	function fallbackRender({ error, resetErrorBoundary }) {
		return (
			<div role="alert" className='alert alert-warning container mt-5'>
				<p>Something went wrong:</p>
				<pre className='mb-0'>{error.message}</pre>
			</div>
		);
	}

	return (
		<ErrorBoundary fallbackRender={fallbackRender}>
			<Helmet>
				<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
			</Helmet>
			<PanelNav />
			<App serverLog={serverLog} serverStatus={serverStatus} />
			<aside>
				{serverError && (
					<ErrorPopup error={serverError} onClose={handleErrorClose} delay={60000} />
				)}
			</aside>
		</ErrorBoundary>
	);
}

export default function AppWrapper() {
	return (
		<LanguageProvider>
			<WrappedApp />
		</LanguageProvider>
	);
}
