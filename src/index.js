import React from 'react';
import { createRoot } from 'react-dom/client';
import App from './App.jsx';
import LoadingApp from './LoadingApp.jsx';
import '../node_modules/bootstrap/dist/js/bootstrap.bundle.js';
// @todo fix the double <style> injection
import './scss/index.scss';
import './assets/icomoon/style.css';

const $root = document.getElementById('root');
if ($root) {
	const root = createRoot($root);
	root.render(
		<React.StrictMode>
			<App />
		</React.StrictMode>
	);
}
const $loading = document.getElementById('loading-app');
if ($loading) {
	const root = createRoot($loading);
	root.render(
		<LoadingApp />
	);
}