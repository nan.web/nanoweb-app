import React, { useEffect, useState } from 'react';
import { fetchHtml } from '../context/Url';
import { useLanguageContext } from '../context/LanguageContext';

const HtmlContent = ({ url, prefix = '' }) => {
	const [content, setContent] = useState('');
	const { language: _ } = useLanguageContext();

	useEffect(() => {
		async function loadContent() {
			const words = [''];
			if (prefix) words.push(prefix);
			words.push(document.documentElement.lang);
			words.push(url);
			const directUrl = url.startsWith('/') || url.startsWith('http://') || url.startsWith('https://') || url.startsWith('://')
				? url
				: words.join('/');
			try {
				const htmlContent = await fetchHtml(directUrl);
				setContent(htmlContent);
			} catch (error) {
				console.error(error);
				setContent(
					'<div class="alert alert-danger">' +
						'<i class="icon-wifi-off"></i>' +
						'<span>' +
							( _['HtmlContentErrorLoad'] || 'Failed to load content' ) +
						'</span>' +
					'</div>'
				);
			}
		}
		loadContent();
	}, [url]);

	return (
		<div dangerouslySetInnerHTML={{ __html: content }} />
	);
};

export default HtmlContent;
