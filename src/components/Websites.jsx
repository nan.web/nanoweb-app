import React, { useState, useRef, useEffect, act } from 'react';
import { Button, Form, ListGroup, Modal, InputGroup, DropdownButton, Dropdown } from 'react-bootstrap';
import { useLanguageContext } from '../context/LanguageContext';
import { extractName } from '../context/Url';
import Icon from './Icon';
import HtmlContent from './HtmlContent';

const Website = ({ website, dir, status, onSelect, onEdit, onSettings, onRemove, isActive }) => {
	const { language: _ } = useLanguageContext();
	return (
		<ListGroup.Item active={isActive}>
			<div className="d-flex justify-content-between align-items-center gap-2 gap-lg-3">
				{dir?.icon && (
					<img className='website-logo' src={dir.icon} alt={website.HOST} />
				)}
				{dir?.url ? (
					<a href={dir.url} target='_blank' rel='noopener nofollow' className='btn btn-info'>
						<Icon name={extractName(dir.url).toLowerCase()} />
					</a>
				) : (
					<span className={`btn ${dir?.git ? 'btn-success' : 'btn-danger'}`}>
						{dir?.git ? (
							<Icon name='wifi' />
						) : (
							<Icon name='wifi-off' />
						)}
					</span>
				)}
				<a href={website.HOST} style={{ flex: '1' }} target="_blank" rel="noopener nofollow">{website.HOST}</a>
				{status ? (
					<span className={`badge bg-${status === 'started' ? 'success' : 'warning'}`}>:{website.SERVER_PORT}</span>
				) : (
					<span className='badge bg-secondary'>:{website.SERVER_PORT}</span>
				)}
				{dir?.nanoweb ? (
					<Button variant="secondary" onClick={() => onEdit(website)}>
						<Icon name='edit' />
						<span>{_['WebsitesEditBtn'] || 'Edit'}</span>
					</Button>
				) : (
					<Button variant="secondary" onClick={() => onSettings(website)}>
						<Icon name='settings' />
						<span>{_['WebsitesConfigureBtn'] || 'Config'}</span>
					</Button>
				)}
				<div className='d-flex gap-2'>
					<DropdownButton id="dropdown-basic-button" title={_['WebsitesActionsBtn'] || 'Actions'} size="sm">
						<Dropdown.Item onClick={() => onSettings(website)}>
							<Icon name='settings' />
							<span>{_['WebsitesSettingsBtn'] || 'Settings'}</span>
						</Dropdown.Item>
						<Dropdown.Divider />
						<Dropdown.Item onClick={() => onRemove(website)} className='text-danger'>
							<Icon name='remove' />
							<span>{_['WebsitesRemoveBtn'] || 'Remove'}</span>
						</Dropdown.Item>
					</DropdownButton>
				</div>
			</div>
		</ListGroup.Item>
	);
};

const EditWebsite = ({ show, handleClose, handleSave, website, dir }) => {
	const { language: _ } = useLanguageContext();
	const [formData, setFormData] = useState({ HOST: '', SERVER_PORT: '', ROOT_DIR: '' });
	const [initialUrl, setInitialUrl] = useState('');
	const [currentUrl, setCurrentUrl] = useState('');
	const firstInputRef = useRef(null);

	useEffect(() => {
		if (website) {
			setFormData(website);
			if (dir?.url) {
				setInitialUrl(dir.url);
				setCurrentUrl(dir.url);
			}
		}
	}, [website, dir]);

	useEffect(() => {
		if (show && firstInputRef.current) {
			firstInputRef.current.focus();
		}
	}, [show]);

	const handleInputChange = (e) => {
		const { name, value } = e.target;
		setFormData({ ...formData, [name]: value });
	};

	const handleDirectorySelection = async (key, dir) => {
		try {
			const directoryPath = await window.api.selectDirectory(dir);
			setFormData({
				...formData,
				[key]: directoryPath
			});
		} catch (error) {
			console.error(error);
		}
	};

	const onInitGit = (e) => {
		window.api.gitInit(website, dir);
		window.api.receive('git-init-success', (message) => {
			console.log(message); // Update your UI accordingly
		});
		window.api.receive('git-init-error', (message) => {
			console.error(message); // Update your UI accordingly
		});
	};

	const onConnectGithub = (e) => {
		console.log('Connecting ..', window.api.gitConnect2Github);
		window.api.gitConnect2Github(website, dir);
		window.api.receive('git-connect-github-success', (message) => {
			console.log(message); // Update your UI accordingly
		});

		window.api.receive('git-connect-github-error', (message) => {
			console.error(message); // Update your UI accordingly
		});
	};
	
	const onConnectGitlab = (e) => {
		window.api.gitConnect2Gitlab(website, dir);
		window.api.receive('git-connect-gitlab-success', (message) => {
			console.log(message); // Update your UI accordingly
		});

		window.api.receive('git-connect-gitlab-error', (message) => {
			console.error(message); // Update your UI accordingly
		});
	};

	const onSaveUrl = () => {
		window.api.gitSaveUrl(website, dir, currentUrl);
		window.api.receive('git-save-url-success', (message) => {
			console.log(message); // Update your UI accordingly
		});

		window.api.receive('git-save-url-error', (message) => {
			console.error(message); // Update your UI accordingly
		});
	};

	const handleUrlChange = (e) => {
		setCurrentUrl(e.target.value);
	};

	const handleSubmit = () => {
		handleSave(formData);
		setFormData({ HOST: '', SERVER_PORT: '', ROOT_DIR: '' });
		handleClose();
	};

	return (
		<Modal size='lg' show={show} onHide={handleClose}>
			<Modal.Header closeButton>
				<Modal.Title>{website ? (_['WebsitesEditTitle'] || 'Edit Website') : (_['WebsitesAddTitle'] || 'Add Website')}</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form>
					<Form.Group controlId="formHost">
						<Form.Label>{_['Websites_HOST'] || 'HOST'}</Form.Label>
						<Form.Control
							type="text"
							name="HOST"
							placeholder={_['Websites_HOST_Placeholder'] || 'Enter HOST'}
							value={formData.HOST}
							onChange={handleInputChange}
							ref={firstInputRef}
						/>
					</Form.Group>
					<Form.Group controlId="formServerPort" className="mt-3">
						<Form.Label>{_['Websites_SERVER_PORT'] || 'SERVER_PORT'}</Form.Label>
						<Form.Control
							type="text"
							name="SERVER_PORT"
							placeholder={_['Websites_SERVER_PORT_Placeholder'] || 'Enter SERVER_PORT'}
							value={formData.SERVER_PORT}
							onChange={handleInputChange}
						/>
					</Form.Group>
					<Form.Group controlId="formRootDir" className="mt-3">
						<Form.Label>{_['Websites_ROOT_DIR'] || 'ROOT_DIR'}</Form.Label>
						<InputGroup>
							<Form.Control
								type="text"
								name="ROOT_DIR"
								placeholder={_['Websites_ROOT_DIR_Placeholder'] || 'Enter ROOT_DIR'}
								value={formData.ROOT_DIR}
								onChange={handleInputChange}
								readOnly
							/>
							<Button variant="secondary" onClick={() => { return handleDirectorySelection('ROOT_DIR', formData.ROOT_DIR)} }>
								{_['WebsitesSelectDir'] || 'Select directory'}
							</Button>
						</InputGroup>
					</Form.Group>
					<div className='py-5'>
						<h2 className='h5 text-center'>{_['WebsitesGitSectionTitle'] || 'Git settings'}</h2>
						<Form.Group controlId="formUrl" className="mt-3">
							<Form.Label>{_['Websites_URL'] || 'URL'}</Form.Label>
							<InputGroup>
								<Form.Control
									type="url"
									name="URL"
									placeholder={_['Websites_URL_Placeholder'] || 'Enter URL'}
									value={currentUrl}
									onChange={handleUrlChange}
								/>
								<Button 
									variant='success' 
									onClick={onSaveUrl}
									disabled={currentUrl === initialUrl}
								>
									<Icon name='save' />
									{_['WebsitesSaveGitUrl'] || 'Save'}
								</Button>
							</InputGroup>
						</Form.Group>
						<div className="mt-3 d-flex flex-wrap justify-content-center gap-2">
						<Button variant="secondary" onClick={onInitGit} disabled={dir?.git}>
								<Icon name='git' />
								{_['WebsitesGitInitBtn'] || 'Init Git'}
							</Button>
							{/* <Button variant="secondary" onClick={onInitGit} disabled={!dir?.git}>
								<Icon name='git' />
								{_['WebsitesGitPullBtn'] || 'Pull the data'}
							</Button> */}
							{/* <Button variant="secondary" onClick={onConnectGithub}>
								<Icon name='github' />
								{_['WebsitesConnectGitHub'] || 'Connect to GitHub'}
							</Button>
							<Button variant="secondary" onClick={onConnectGitlab}>
								<Icon name='gitlab' />
								{_['WebsitesConnectGitLab'] || 'Connect to GitLab'}
							</Button> */}
						</div>
					</div>
				</Form>
			</Modal.Body>
			<Modal.Footer className='d-flex flex-wrap justify-content-between'>
				<Button variant="secondary" onClick={handleClose}>
					<Icon name='close' />
					{_['WebsitesCloseBtn'] || 'Close'}
				</Button>
				<Button variant="primary" onClick={handleSubmit}>
					<Icon name='save' />
					{website ? (_['WebsitesUpdateBtn'] || 'Update Website') : (_['WebsitesAddBtn'] || 'Add Website')}
				</Button>
			</Modal.Footer>
		</Modal>
	);
};

const Websites = ({ websites, dirs, activeWebsite, serverStatus, onSelect, onSave, onRemove }) => {
	const { language: _ } = useLanguageContext();
	const [showModal, setShowModal] = useState(false);
	const [editWebsite, setEditWebsite] = useState(null);
	const [editDir, setEditDir] = useState(null);
	const [status, setStatus] = useState([]);

	const handleSave = (updatedWebsite) => {
		const updatedWebsites = [...websites];
		if (editWebsite !== null) {
			updatedWebsites[websites.indexOf(editWebsite)] = updatedWebsite;
		} else {
			updatedWebsites.push(updatedWebsite);
		}
		onSave(updatedWebsites);
	};

	const handleEdit = (website, index) => {
		setEditWebsite(website);
		setEditDir(dirs[index] || null);
		const target = { website, dir: dirs[index], index };
		onSelect(target);
	};

	const handleSettings = (website, index) => {
		setEditWebsite(website);
		setEditDir(dirs[index] || null);
		setShowModal(true);
	};

	const handleRemove = (website, index) => {
		const updatedWebsites = websites.filter(w => w !== website);
		const target = { website, dir: dirs[index], index, updatedWebsites };
		onRemove(target);
	};

	useEffect(() => {
		const st = [];
		for (const website of websites) st.push(serverStatus?.[website?.ROOT_DIR] || '');
		setStatus(st);
	}, [serverStatus, websites]);

	return (
		<section className="websites container">
			<h1 className='mb-0'>{_['WebsitesTitle'] || 'Manage Websites'}</h1>
			<ListGroup>
				{websites.map((website, index) => (
					<Website
						key={index}
						website={website}
						dir={dirs[index]}
						status={status[index]}
						onSelect={onSelect}
						onEdit={() => handleEdit(website, index)}
						onSettings={() => handleSettings(website, index)}
						onRemove={() => handleRemove(website, index)}
						isActive={index === activeWebsite}
					/>
				))}
			</ListGroup>
			{websites.length === 0 && (
				<HtmlContent url='websites/empty.html' prefix='docs' />
			)}
			<Button variant="primary" onClick={() => setShowModal(true)}>
				<Icon name='plus' />
				<span>{_['WebsitesAddBtn'] || 'Add Website'}</span>
			</Button>

			<EditWebsite
				show={showModal}
				handleClose={() => setShowModal(false)}
				handleSave={handleSave}
				website={editWebsite}
				dir={editDir}
			/>
		</section>
	);
};

export default Websites;
