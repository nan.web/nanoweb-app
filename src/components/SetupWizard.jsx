import React, { useEffect, useState } from 'react';
import { useLanguageContext } from '../context/LanguageContext';

const SetupWizard = ({ setConfig }) => {
	const { language: _ } = useLanguageContext();
	const [config, setLocalConfig] = useState({
		ROOT_DIR: '',
		// DATA_DIR: '',
		// DIST_DIR: '',
		// LOGS_DIR: '',
		// NANO_DIR: '',
		// PLUGINS_DIR: '',
		// STATIC_DIR: '',
		// THEMES_DIR: '',
		HOST: '',
		SERVER_PORT: 3006,
	});

	const handleDirectorySelection = async (key) => {
		const directoryPath = await window.api.selectDirectory();
		setLocalConfig({
			...config,
			[key]: directoryPath
		});
	};

	const handleChange = (e) => {
		setLocalConfig({
			...config,
			[e.target.name]: e.target.value
		});
	};

	const saveConfig = (e) => {
		e.preventDefault();
		// Save the config to nanoweb.config.json
		window.api.saveConfig(config);
		setConfig(config);
	};

	useEffect(() => {
		const setDefaultRootDir = async () => {
			const currentDir = await window.api.getCurrentDirectory();
			setLocalConfig(config => ({
				...config,
				ROOT_DIR: currentDir
			}));
		};
		setDefaultRootDir();
	}, []);

	return (
		<section className='container align-items-center text-center gap-5'>
			<h1>{_['SetupWizardTitle'] || 'Setup Wizard'}</h1>
			<form className='wide' onSubmit={saveConfig}>
				{Object.keys(config).map((key) => (
					<div className='mb-3' key={key}>
						<label className='form-label text-start'>
							{_['SetupWizard_' + key] || key.replace('_', ' ')}
						</label>
						<div className='input-group'>
							<input
								type='text'
								className='form-control'
								name={key}
								value={config[key]}
								onChange={handleChange}
								placeholder={_['SetupWizard_' + key + '_Placeholder'] || key.replace('_', ' ')}
								readOnly={key.endsWith('_DIR')}
							/>
							{key.endsWith('_DIR') && (
								<button
									type='button'
									className='btn btn-secondary'
									onClick={() => handleDirectorySelection(key)}
								>
									{_['SetupWizardSelectDir'] || 'Select directory'}
								</button>
							)}
						</div>
					</div>
				))}
				<button type='submit' className='btn btn-success'>
					{_['SetupWizardSaveBtn'] || 'Save Configuration'}
				</button>
			</form>
		</section>
	);
};

export default SetupWizard;
