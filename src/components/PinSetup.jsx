import React, { useState } from 'react';
import { useLanguageContext } from '../context/LanguageContext';

const PinSetup = ({ setPin }) => {
	const { language: _ } = useLanguageContext();
	const [pin, setLocalPin] = useState('');

	const savePin = () => {
		// Save the pin securely
		setPin(pin);
	};

	return (
		<section className='container align-items-center text-center gap-5'>
			<h1>{_['PinSetupTitle'] || 'Set up PIN'}</h1>
			<form className='short'>
				<div className='input-group input-group-lg'>
					<input className='form-control' type="text" value={pin} onChange={(e) => setLocalPin(e.target.value)} />
					<button className='btn btn-primary' onClick={savePin}>{_['PinSetupBtn'] || 'Set'}</button>
				</div>
			</form>
		</section>
	);
};

export default PinSetup;
