import React, { useEffect, useState, useRef } from 'react';
import { Button } from 'react-bootstrap';
import { useLanguageContext } from '../../context/LanguageContext';
import Icon from '../Icon';

const CommitForm = () => {
	const { language: _ } = useLanguageContext();
	return (
		<form>
			<textarea className='form-control'></textarea>
			<Button type='submit' variant='primary'>
				<Icon name='share' />
				<span>{_['PublisherCommitBtn'] || 'Commit'}</span>
			</Button>
		</form>
	);
};

export default CommitForm;