import React, { useEffect, useState, useRef } from 'react';
import { useLanguageContext } from '../../context/LanguageContext';
import { Accordion, InputGroup } from 'react-bootstrap';

const GIT = {
	NOT_ADDED: 'N',
	CONFLICTED: 'C',
	CREATED: 'A',
	DELETED: 'D',
	MODIFIED: 'M',
	RENAMED: 'R',
	STAGED: 'S',
	DIRECTORY: '.',
};

const GitManger = () => {
	const { activeWebsite, language: _ } = useLanguageContext();
	const [gitChanges, setGitChanges] = useState({});
	const [selectedFiles, setSelectedFiles] = useState({});
	const itemRefs = useRef([]);

	useEffect(() => {
		const handleGitReadSuccess = ({ changes, index }) => {
			setGitChanges((prev) => ({ ...prev, [index]: changes }));
		};

		const handleGitReadFail = (error) => {
			console.error('Failed to read git status:', error);
		};

		window.api.onGitReadSuccess(handleGitReadSuccess);
		window.api.onGitReadFail(handleGitReadFail);
		return () => {
			window.api.removeGitReadResponse();
			window.api.removeGitReadFail();
		};
	}, [activeWebsite]);

	useEffect(() => {
		console.log(itemRefs);
	}, [itemRefs]);

	const handleKeyDown = (event) => {
		const { key } = event;
		const activeElementIndex = itemRefs.current.findIndex(el => el === document.activeElement);

		if (key === 'ArrowDown') {
			const nextIndex = activeElementIndex + 1;
			if (nextIndex < itemRefs.current.length) {
				itemRefs.current[nextIndex].focus();
			}
		} else if (key === 'ArrowUp') {
			const prevIndex = activeElementIndex - 1;
			if (prevIndex >= 0) {
				itemRefs.current[prevIndex].focus();
			}
		} else if (key === 'ArrowLeft') {
			const prevIndex = activeElementIndex - 1;
			if (prevIndex >= 0) {
				itemRefs.current[prevIndex].focus();
			}
		} else if (key === 'ArrowRight') {
			const prevIndex = activeElementIndex - 1;
			if (prevIndex >= 0) {
				itemRefs.current[prevIndex].focus();
			}
		} else if (key === 'Enter') {
			const currentItem = itemRefs.current[activeElementIndex];
			const input = currentItem.querySelector('input[type="checkbox"]');
			if (input) {
				input.click();
			} else {
				currentItem.querySelector('.accordion-button').click();
			}
		} else if (key === ' ') {
			event.preventDefault();
			const currentItem = itemRefs.current[activeElementIndex];
			currentItem.querySelector('.accordion-button').click();
		}
	};

	const toggleFileSelection = (filePath) => {
		setSelectedFiles((prev) => ({
			...prev,
			[filePath]: !prev[filePath],
		}));
	};

	const formatChangesToTree = (changes) => {
		const tree = {};
		if (!changes) return tree;

		const addFilesToTree = (files, type) => {
			files.forEach((file) => {
				const parts = file.path ? file.path.split('/') : file.split('/');
				let current = tree;
				parts.forEach((part, index) => {
					if (!current[part]) {
						current[part] = { __type: index === parts.length - 1 ? type : 'dir', __files: [] };
					}
					current = current[part];
				});
			});
		};

		addFilesToTree(changes.not_added, GIT.NOT_ADDED);
		addFilesToTree(changes.conflicted, GIT.CONFLICTED);
		addFilesToTree(changes.created, GIT.CREATED);
		addFilesToTree(changes.deleted, GIT.DELETED);
		addFilesToTree(changes.modified, GIT.MODIFIED);
		addFilesToTree(changes.renamed, GIT.RENAMED);
		addFilesToTree(changes.staged, GIT.STAGED);

		return tree;
	};

	const renderTree = (node, path = '', parentIndex = '', treeId = '') => {
		if (!node) return null;
		return Object.keys(node).map((key, index) => {
			if (key === '__type' || key === '__files') return null;
			const currentPath = `${path}/${key}`;
			const currentIndex = `${parentIndex}${index}`;
			const id = treeId + '-' + index;
			if (node[key].__type === 'dir') {
				return (
					<Accordion key={currentPath} ref={el => (itemRefs.current[currentIndex] = el)} tabIndex={0}>
						<Accordion.Item eventKey={currentPath}>
							<Accordion.Header>
								<InputGroup>
									<InputGroup.Checkbox
										id={`file-${treeId + index}`}
										aria-label={_['PublisherCheckFileForCommit'] || 'Check file for the commit'}
										checked={selectedFiles[currentPath] || false}
										onChange={() => toggleFileSelection(currentPath)}
									/>
									<label htmlFor={`file-${id}`} className='form-control'>{key}</label>
								</InputGroup>
							</Accordion.Header>
							<Accordion.Body>
								{renderTree(node[key], currentPath, `${currentIndex}-`, id)}
							</Accordion.Body>
						</Accordion.Item>
					</Accordion>
				);
			}
			return (
				<InputGroup key={currentPath} ref={el => (itemRefs.current[currentIndex] = el)} tabIndex={0} className='file'>
					<InputGroup.Checkbox
						id={`file-${id}`}
						aria-label={_['PublisherCheckFileForCommit'] || 'Check file for the commit'}
						checked={selectedFiles[currentPath] || false}
						onChange={() => toggleFileSelection(currentPath)}
					/>
					<label htmlFor={`file-${id}`} className='form-control'>{key}</label>
					<span className='form-control type' title={_[`PublisherGitType_${node[key].__type}`] || `PublisherGitType_${node[key].__type}`}>
						{node[key].__type}
					</span>
				</InputGroup>
			);
		});
	};

	return (
		<div className='changes' onKeyDown={handleKeyDown} tabIndex={0}>
			{renderTree(formatChangesToTree(gitChanges[activeWebsite]))}
		</div>
	);
};

export default GitManger;