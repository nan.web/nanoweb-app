import React, { useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';
import { useLanguageContext } from '../../context/LanguageContext';

const WebsiteSelector = ({ selectWebsite }) => {
	const { config, activeWebsite, setActiveWebsite, language: _ } = useLanguageContext();
	const [selectedIndex, setSelectedIndex] = useState(activeWebsite);

	useEffect(() => {
		selectWebsite(selectedIndex);
	}, [selectedIndex, setActiveWebsite]);

	if (!config?.websites?.length) return (<span>{_['Loading..'] || 'Loading ..'}</span>);

	return (
		<Form.Select
			value={selectedIndex}
			onChange={(e) => setSelectedIndex(parseInt(e.target.value, 10))}
		>
			{config.websites.map((website, index) => (
				<option key={index} value={index}>{website.HOST} • {website.ROOT_DIR}</option>
			))}
		</Form.Select>
	);
};

export default WebsiteSelector;
