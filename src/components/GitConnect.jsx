import React, { useState } from 'react';

const GitConnect = () => {
	const [platform, setPlatform] = useState('');
	const [authMethod, setAuthMethod] = useState('sso');
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState('');
	const [message, setMessage] = useState('');

	const handlePlatformChange = (event) => {
		setPlatform(event.target.value);
		setError('');
		setMessage('');
	};

	const handleAuthMethodChange = (event) => {
		setAuthMethod(event.target.value);
		setError('');
		setMessage('');
	};

	const handleLogin = async (event) => {
		event.preventDefault();
		setIsLoading(true);

		if (authMethod === 'sso') {
			// Redirect to SSO
			const authUrl = platform === 'github'
				? '/auth/github'
				: '/auth/gitlab';
			window.location.href = authUrl;
		} else {
			// Login with username and password
			try {
				const response = await fetch('/api/login', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
					},
					body: JSON.stringify({ platform, username, password }),
				});
				const data = await response.json();
				if (response.ok) {
					setMessage(data.message);
				} else {
					setError('Login failed. Please check your credentials.');
				}
			} catch (error) {
				setError('An error occurred. Please try again.');
			} finally {
				setIsLoading(false);
			}
		}
	};

	return (
		<section className="container">
			<div className="row justify-content-md-center">
				<div className="col-md-6">
					<h2 className="my-4">Connect to a Platform</h2>
					<form onSubmit={handleLogin}>
						<div className="mb-3">
							<label htmlFor="platformSelect" className="form-label">Select Platform</label>
							<select className="form-select" id="platformSelect" value={platform} onChange={handlePlatformChange} required>
								<option value="">Choose...</option>
								<option value="github">GitHub</option>
								<option value="gitlab">GitLab</option>
							</select>
						</div>

						<div className="mb-3">
							<label htmlFor="authMethod" className="form-label">Authentication Method</label>
							<select className="form-select" id="authMethod" value={authMethod} onChange={handleAuthMethodChange} required>
								<option value="sso">Single Sign-On (SSO)</option>
								<option value="login">Login with Username and Password</option>
							</select>
						</div>

						{authMethod === 'login' && (
							<>
								<div className="mb-3">
									<label htmlFor="username" className="form-label">Username</label>
									<input
										type="text"
										className="form-control"
										id="username"
										placeholder="Enter your username"
										value={username}
										onChange={(e) => setUsername(e.target.value)}
										required
									/>
								</div>

								<div className="mb-3">
									<label htmlFor="password" className="form-label">Password</label>
									<input
										type="password"
										className="form-control"
										id="password"
										placeholder="Enter your password"
										value={password}
										onChange={(e) => setPassword(e.target.value)}
										required
									/>
								</div>
							</>
						)}

						{error && <div className="alert alert-danger mt-3">{error}</div>}
						{message && <div className="alert alert-success mt-3">{message}</div>}

						<button type="submit" className="btn btn-primary" disabled={isLoading}>
							{isLoading ? (
								<span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
							) : 'Connect'}
						</button>
					</form>
				</div>
			</div>
		</section>
	);
};

export default GitConnect;
