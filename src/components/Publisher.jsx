import React, { useEffect, useState } from 'react';
import { useLanguageContext } from '../context/LanguageContext';
import WebsiteSelector from './Publisher/WebsiteSelector';
import CommitForm from './Publisher/CommitForm';
import GitManager from './Publisher/GitManager';

const Publisher = () => {
	const { config, activeWebsite, setActiveWebsite, language: _ } = useLanguageContext();
	const [websites, setWebsites] = useState([]);

	useEffect(() => {
		const loadConfig = () => {
			if (!config.websites) return;
			const webs = [];
			config.websites.forEach((website, index) => {
				webs.push({
					...website,
					dir: config.dirs[index],
				});
			});
			setWebsites(webs);
		};
		loadConfig();
	}, [config]);

	useEffect(() => {
		const readGit = () => {
			const website = websites[activeWebsite];
			if (!website) return;
			window.api.readGit({ website, dir: website.dir, index: activeWebsite });
		};
		readGit();
	}, [activeWebsite, websites]);

	const selectWebsite = (index) => {
		setActiveWebsite(index, true);
	};

	return (
		<section className='publisher container'>
			<h1 className='mb-0'>{_['PublisherTitle'] || 'Publish websites'}</h1>
			<div className='body'>
				<div className='manager'>
					<WebsiteSelector selectWebsite={selectWebsite} />
					<GitManager />
				</div>
				<aside className='options'>
					<CommitForm />
				</aside>
			</div>
		</section>
	);
};

export default Publisher;
