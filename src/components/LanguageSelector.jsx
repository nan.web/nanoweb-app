import React, { useState } from 'react';
import { useLanguageContext } from '../context/LanguageContext';
import Loading from './Loading';

const LanguageSelector = () => {
	const { languages, selectLanguage, loading } = useLanguageContext();
	const [heading, setHeading] = useState('Select Language');

	const handleMouseEnter = (selectMessage) => {
		setHeading(selectMessage);
	};

	if (loading) {
		return <Loading />;
	}

	return (
		<section className='container text-center gap-5'>
			<h1>{heading}</h1>
			<div className='d-flex flex-wrap justify-content-center gap-3'>
				{languages.map(language => (
					<button
						key={language.locale}
						className="btn btn-lg btn-info d-flex align-items-center gap-2"
						onClick={() => selectLanguage(language.locale)}
						onMouseEnter={() => handleMouseEnter(language.select)}
					>
						{language.icon && (
							<span
								dangerouslySetInnerHTML={{ __html: language.icon }}
								style={{ maxHeight: '2rem', maxWidth: '3rem' }}
							/>
						)}
						{language.title}
					</button>
				))}
			</div>
		</section>
	);
};

export default LanguageSelector;
