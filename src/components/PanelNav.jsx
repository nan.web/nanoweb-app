import React, { useEffect, useState } from 'react';
import { useLanguageContext } from '../context/LanguageContext';
import { Button } from 'react-bootstrap';
import Icon from './Icon';

export default function PanelNav() {
	const { language: _, selectLanguage, config, hideWebsite, setActiveWebsite, setPage } = useLanguageContext();
	const navWebsites = () => {
		hideWebsite();
		setPage('websites');
	};

	const navLanguages = () => {
		hideWebsite();
		selectLanguage('');
		setPage('languages');
	};

	const navPublish = () => {
		hideWebsite();
		setPage('publish');
	};

	const navQuit = () => {
		hideWebsite();
		window.api.quit();
	};

	return (
		<nav className='root'>
			<Button variant="secondary" size='lg' title={_['WebsitesNav'] || 'Websites'} onClick={navWebsites}>
				<Icon name='nanoweb' />
				<span>{_['WebsitesNav'] || 'Websites'}</span>
			</Button>
			{config?.websites && (
				config.websites.map((website, index) => (
					<button key={index} className='btn btn-lg' title={website.HOST} onClick={() => setActiveWebsite(index)}>
						{config?.dirs[index]?.icon ? (
							<img src={config?.dirs[index]?.icon} alt={website.host} />
						) : (
							<Icon name='nanoweb' className='text-warning' />
						)}
					</button>
				))
			)}
			<span className='btn btn-lg divider'>∙</span>
			<Button variant="secondary" size='lg' title={_['PublishNav'] || 'Publish website'} onClick={navPublish}>
				<Icon name='share' />
				<span>{_['PublishNav'] || 'Publish website'}</span>
			</Button>
			<span className='btn btn-lg divider'>∙</span>
			<Button variant="secondary" size='lg' title={_['LanguagesNav'] || 'Select Language'} onClick={navLanguages}>
				<Icon name='globe' />
				<span>{_['LanguagesNav'] || 'Select Language'}</span>
			</Button>
			<Button variant="secondary" className='mt-auto' size='lg' title={_['QuitNav'] || 'Quit'} onClick={navQuit}>
				<Icon name='power' />
				<span>{_['QuitNav'] || 'Quit'}</span>
			</Button>
		</nav>
	);
}


