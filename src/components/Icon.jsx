import React from 'react';

const icons = {
	close: 'times',
	remove: 'trash',
	git: 'dir-tree'
};
const Icon = ({ name, className = '', ...props }) => {
	const correctIcon = icons[name] || name;
	return (
		<i className={`icon-${correctIcon} ${className}`} {...props} />
	);
};

export default Icon;
