import React, { useEffect } from 'react';
import { useLanguageContext } from '../context/LanguageContext';

function ErrorPopup({ error, onClose, delay = 60000 }) {
	const { language: _ } = useLanguageContext();
	useEffect(() => {
		const timer = setTimeout(() => {
			onClose();
		}, delay);

		return () => clearTimeout(timer);
	}, [onClose]);

	return (
		<div className="popup-error sticky-bottom container alert alert-danger">
			<pre>{error}</pre>
			<button className='btn btn-close' onClick={onClose} title={_['CloseBtn'] || 'Close'}></button>
		</div>
	);
}

export default ErrorPopup;