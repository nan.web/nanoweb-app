import React from 'react';

const Loading = () => {
	// const circle = '#339300';
	const circle = '#fff';
	return (
		<div className='loading'>
			<svg className='loading-icon' width="333" height="333" viewBox="0 0 333 333" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M333 166.5C333 258.455 258.455 333 166.5 333C74.5446 333 0 258.455 0 166.5C0 74.5446 74.5446 0 166.5 0C258.455 0 333 74.5446 333 166.5Z" fill="black"/>
				<path fillRule="evenodd" clipRule="evenodd" d="M166.5 312.818C247.309 312.818 312.818 247.309 312.818 166.5C312.818 85.6907 247.309 20.1818 166.5 20.1818C85.6907 20.1818 20.1818 85.6907 20.1818 166.5C20.1818 247.309 85.6907 312.818 166.5 312.818ZM166.5 333C258.455 333 333 258.455 333 166.5C333 74.5446 258.455 0 166.5 0C74.5446 0 0 74.5446 0 166.5C0 258.455 74.5446 333 166.5 333Z" fill={circle}/>
				<path fillRule="evenodd" clipRule="evenodd" d="M219.356 207.477L166.5 154.185V131.182L242.182 207.489L166.5 282.545V259.896L219.356 207.477Z" fill="#0099DC"/>
				<path fillRule="evenodd" clipRule="evenodd" d="M113.644 141.886L166.5 88.5938V65.5909L90.8182 141.898L166.5 216.955L166.5 194.305L113.644 141.886Z" fill="#F9F909"/>
			</svg>
		</div>
	);
};

export default Loading;
