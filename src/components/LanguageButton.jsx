import React, { useEffect, useState } from 'react';
import { useLanguageContext } from '../context/LanguageContext';

const LanguageButton = () => {
	const { languages, language: _, locale, selectLanguage } = useLanguageContext();
	const [lang, setLang] = useState({
		locale: '',
		select: '',
		icon: null,
		title: ''
	});
	useEffect(() => {
		(languages || []).forEach(item => {
			if (item.locale === locale) setLang(item);
		});
		return () => {};
	}, []);

	const onClick = () => {
		selectLanguage(null);
	};

	return (
		<button className="btn btn-lg btn-info d-flex align-items-center gap-2" onClick={onClick}>
			{lang.icon && (
				<span
					dangerouslySetInnerHTML={{ __html: lang.icon }}
					style={{ maxHeight: '2rem', maxWidth: '3rem' }}
				/>
			)}
			{lang.title || _['LanguageSelectBtn'] || 'Select'}
		</button>
	);
};

export default LanguageButton;
