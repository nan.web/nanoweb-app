import React, { useEffect, useState } from 'react';
import { LanguageProvider, useLanguageContext } from './context/LanguageContext';
import Loading from './components/Loading';
import ErrorPopup from './components/ErrorPopup';
import { ErrorBoundary } from "react-error-boundary";
import { setupExternalLinkHandler } from './context/Url';

function LoadingApp({ serverLog }) {
	const { setLoading, language: _ } = useLanguageContext();

	useEffect(() => {
		setupExternalLinkHandler();
	}, []);

	useEffect(() => {
		const handleSuccess = ({ win, url, website, dir, index }) => {
			console.log(win, url, website, dir, index);
			setLoading(false);
		};
		window.api.onWebsiteStartSuccess(handleSuccess);

		return () => {
			window.api.removeWebsiteStartSuccess(handleSuccess);
		};
	}, []);

	return (
		<main className='loading'>
			<Loading />
			{serverLog.length > 0 && (
				<div className="list-group container server-log">
					{serverLog.map((log, index) => (
						<div key={index} className='list-group-item'>
							<p className={`badge ${log.type === 'stderr' ? 'bg-danger' : 'bg-secondary'}`}>{log.website.ROOT_DIR}</p>
							<pre>{log.data.trim()}</pre>
						</div>
					))}
				</div>
			)}
		</main>
	);
}

export default function WrappedApp() {
	const [serverLog, setServerLog] = useState([]);

	function showWebsite(website) {
		window.api.showWebsite(website);
	}

	useEffect(() => {
		const handleError = ({ error, website, dir, index }) => {
			console.error(error, website);
			if (/EADDRINUSE/.test(error) && (new RegExp(`:::${website.SERVER_PORT}`)).test(error)) {
				return showWebsite();
			}
		};
		window.api.onWebsiteStartFail(handleError);

		return () => {
			window.api.removeWebsiteStartFail(handleError);
		};
	}, []);

	useEffect(() => {
		const handleLog = ({ type, data, website }) => {
			console.log(data, website);
			setServerLog(prevLogs => [...prevLogs, { type, data, website }]);
			const text = data.trim();
			if (text.startsWith('Server started at')) {
				showWebsite(website);
			}
			if (text.includes('EADDRINUSE') && type === 'stderr') {
				showWebsite(website);
			}
		};
		window.api.onWebsiteStartLog(handleLog);

		return () => {
			window.api.removeWebsiteStartLog(handleLog);
		};
	}, []);

	function fallbackRender({ error, resetErrorBoundary }) {
		return (
			<div role="alert" className='alert alert-warning container'>
				<p>Something went wrong:</p>
				<pre className='text-danger'>{error.message}</pre>
			</div>
		);
	}

	return (
		<ErrorBoundary fallbackRender={fallbackRender}>
			<LanguageProvider>
				<LoadingApp serverLog={serverLog} />
			</LanguageProvider>
		</ErrorBoundary>
	);
}
