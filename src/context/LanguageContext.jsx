import React, { createContext, useContext, useState, useEffect, useMemo } from 'react';

// Create a context with 'null' as the default value
export const LanguageContext = createContext(null);

export const useLanguageContext = () => useContext(LanguageContext);

export const LanguageProvider = ({ children }) => {
	const [locale, setLocale] = useState('uk');
	const [language, setLanguage] = useState({});
	const [languages, setLanguages] = useState([]);
	const [loading, setLoading] = useState(true);
	const [config, setConfig] = useState({});
	// The application mode = ['app', 'nav']
	const [mode, setModeRaw] = useState('app');
	const [activeWebsite, setActiveWebsite] = useState(-1);
	const [page, setPage] = useState('');

	useEffect(() => {
		fetch('./languages/index.json')
			.then(response => response.json())
			.then(data => {
				setLanguages(data);
				const savedLanguage = localStorage.getItem('selectedLanguage');
				setLoading(false);
				if (savedLanguage) selectLanguage(savedLanguage);
			});
	}, []);

	useEffect(() => {
		window.api.getConfig().then(fetchedConfig => {
			setConfig(fetchedConfig);
			if ('undefined' !== typeof fetchedConfig['activeWebsite']) {
				setActiveWebsite(fetchedConfig.activeWebsite);
			}
		});
	}, []);

	const selectLanguage = (locale) => {
		if (!locale) {
			setLocale('');
			return;
		}
		document.documentElement.lang = locale;
		setLoading(true);
		fetch(`./languages/${locale}.json`)
			.then(response => response.json())
			.then(data => {
				setLocale(locale);
				setLanguage(data);
				localStorage.setItem('selectedLanguage', locale);
				setLoading(false);
			});
	};

	const saveConfig = async (configFn) => {
		const newConfig = 'function' === typeof configFn ? configFn() : configFn;
		setConfig(newConfig);
		window.api.saveConfig(newConfig);
	};

	const setMode = (value) => {
		setModeRaw(value);
		if ('app' === value) {
			document.body.classList.remove('nav');
		} else {
			document.body.classList.add('nav');
		}
	};

	const activateWebsite = async (index, raw = false) => {
		if (raw) {
			setActiveWebsite(index);
			return;
		}
		const updatedConfig = {
			...config,
			activeWebsite: index
		};
		const website = config.websites[index];
		const dir = config.dirs[index];
		setActiveWebsite(index);
		setConfig(updatedConfig);
		if (index >= 0) {
			showWebsite({ website, dir, index });
		} else {
			hideWebsite();
		}
	};

	const showWebsite = ({ website, dir, index }) => {
		setMode('nav');
		window.api.startWebsite({ website, dir, index });
	};

	const hideWebsite = () => {
		setMode('app');
		window.api.hideWebsite();
	}

	const value = useMemo(() => ({
		language,
		setLanguage,
		languages,
		loading,
		setLoading,
		selectLanguage,
		locale,
		setLocale,
		config,
		setConfig: saveConfig, // Use the new saveConfig function
		mode,
		setMode,
		activeWebsite,
		setActiveWebsite: activateWebsite,
		showWebsite,
		hideWebsite,
		page,
		setPage
	}), [language, languages, loading, locale, config, mode, activeWebsite, page]);

	return (
		<LanguageContext.Provider value={value}>
			{children}
		</LanguageContext.Provider>
	);
};
