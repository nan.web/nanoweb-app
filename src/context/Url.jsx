import React from 'react';

export const openExternalLink = (url) => {
	window.api.openExternal(url);
};

export async function fetchHtml(url) {
	const response = await fetch(url);
	if (response.ok) {
		return await response.text();
	} else {
		throw new Error(`Failed to fetch HTML content from ${url}`);
	}
};

let isExternalLinkHandlerSetup = false;
let lastClickTime = 0;
const CLICK_DELAY = 500; // 500 milliseconds

export function setupExternalLinkHandler() {
	if (isExternalLinkHandlerSetup) return;

	const handleExternalLinkClick = (event) => {
		const target = event.target.closest('a');
		const currentTime = new Date().getTime();

		if (target && target.matches('a[target="_blank"][rel~="noopener"][rel~="nofollow"]')) {
			console.log('Open external:', currentTime, lastClickTime, currentTime - lastClickTime < CLICK_DELAY);
			event.preventDefault();
			if (currentTime - lastClickTime < CLICK_DELAY) {
				return;
			}
			lastClickTime = currentTime;
			window.api.openExternal(target.href);
		}
	};

	document.addEventListener('click', handleExternalLinkClick);
	console.log('setupExternalLinkHandler attached');

	isExternalLinkHandlerSetup = true;
}

/**
 * Returns the name of the brand from its URL.
 * @param {string} url 
 * @example 'https://github.com' => 'github'
 *          'https://nanoweb.yaro.page' => 'yaro'
 *          'https://nanoweb.yaro.com.ua' => 'yaro'
 * @returns {string} Brand name extracted from the URL.
 */
export function extractName(url) {
	try {
		const { hostname } = new URL(url);
		const parts = hostname.split('.');
		
		if (parts.length < 2) {
			return parts[0]; // handle cases like 'localhost' or malformed URLs
		}

		// For domains like 'com', 'net', 'org' etc., get the second last part
		// For domains with subdomains, get the second part from the end
		const domainParts = parts.slice(-3);
		if (domainParts.length === 3 && domainParts[1].length <= 3) {
			return domainParts[1];
		}
		return domainParts[0];
	} catch (error) {
		console.error(`Invalid URL: ${url}`, error);
		return '';
	}
}