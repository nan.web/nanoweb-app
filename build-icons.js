import { execSync } from 'node:child_process';
import fs from 'node:fs'
import path from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const iconSource = path.join(__dirname, 'assets', 'nanowebapp-logo-shadow.png'); // Source icon (1024x1024 PNG)
const outputDir = path.join(__dirname, 'build'); // Output directory for generated icons
const assetsDir = path.join(__dirname, 'assets');
const buildDir = path.join(__dirname, 'build');
const trayIcons = [
	'nanoweb-tray-icon-white@2x.png',
	'nanoweb-tray-icon@2x.png',
	'nanoweb-tray-icon@4x.png',
];

// Run electron-icon-builder to generate the icons
execSync(`npx electron-icon-builder --input=${iconSource} --output=${outputDir} --flatten`, { stdio: 'inherit' });

for (const file of trayIcons) {
	fs.copyFileSync(path.join(assetsDir, file), path.join(buildDir, file));
}

console.log('Icons have been generated in', outputDir);
