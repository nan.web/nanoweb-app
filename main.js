import { app, BrowserWindow, dialog, ipcMain, shell } from 'electron';
import path from 'node:path';
import { exec } from 'child_process';
import getPort, { portNumbers } from 'get-port';
import simpleGit from 'simple-git';
import { createServer, startServerWithConfig, stopWebsite } from './lib/server.js';
import { checkRequirements, checkAndGenerateSSLKeys, getState, saveState, saveStore, getStore, removeWebsite } from './lib/app.js';
import { createWindow, adjustWindowSize, installAddons, createTrayIcon, updateTrayMenu, createAuthWindow } from './lib/startup.js';
import config from './config.js';
import { getGithubUrl, getGitlabUrl, accessToken } from './lib/auth.js';
const { sslKey, sslCert } = config;

const isDev = process.env.NODE_ENV === 'development';

let server = null;
let serverUrl = null;
let port = null;
let tray = null;
let mainWin, websiteWin, mainView, websiteView;
const runningWebsites = {};

async function createMainWindow() {
	const win = await createWindow(serverUrl);
	mainWin = win.win;
	mainView = win.mainView;
	websiteView = win.websiteView;
	websiteView.webContents.on('did-finish-load', () => {});
	websiteView.webContents.on('did-fail-load', (event, errorCode, errorDescription, validatedURL) => {
		console.log('website', errorCode, errorDescription);
		hideWebsite();
	});
}

async function hideWebsite() {
	adjustWindowSize('main', { win: mainWin, mainView, websiteView });
	mainView.webContents.emit('website:on:hide');
}

async function showWebsite(website) {
	websiteView.webContents.loadURL(`http://localhost:${website.SERVER_PORT}`);
	adjustWindowSize('website', { win: mainWin, mainView, websiteView });
}

async function showLoading(website) {
	websiteView.webContents.loadURL(`${serverUrl}/loading.html`);
	adjustWindowSize('website', { win: mainWin, mainView, websiteView });
}

async function onLog({ type, data, website }) {
	const text = data.trim();
	if (text.startsWith('Server started at')) {
		showWebsite(website);
	}
	if (text.includes('EADDRINUSE') && type === 'stderr') {
		showWebsite(website);
	}
}

async function selectWebsite(website) {
	const state = await getState();
	const index = state.websites.findIndex(w => w.HOST === website.HOST);
	if (index !== -1) {
		state.activeWebsite = index;
		saveState(state);
		const activeWebsite = state.websites[state.activeWebsite];
		if (activeWebsite.ROOT_DIR) {
			mainView.webContents.emit('website:on:select');
			showLoading(activeWebsite);
			await updateTrayMenu();
			if (runningWebsites[activeWebsite.ROOT_DIR]) {
				console.log(`Website ${activeWebsite.ROOT_DIR} is already running`);
				showWebsite(activeWebsite);
				return true;
			}
			const sender = BrowserWindow.getAllWindows()[0]?.webContents || null;
			const dir = state?.dirs?.[index] || null;
			runningWebsites[activeWebsite.ROOT_DIR] = true;
			try {
				await startServerWithConfig(activeWebsite, { onLog });
				const url = `http://localhost:${activeWebsite.SERVER_PORT}`;
				dir.port = activeWebsite.SERVER_PORT;
				if (sender) {
					sender.send('website:start:on:success', { url, website, dir, index });
				}
				showWebsite(activeWebsite);
				return true;
			} catch (error) {
				console.error('Start website server error:', error);
				runningWebsites[activeWebsite.ROOT_DIR] = false;
				if (sender) {
					sender.send('website:start:on:fail', { error, website, dir, index });
				}
				return false;
			}
		}
	}
	return true;
}

async function stopAllWebsites() {
	const stopPromises = Object.keys(runningWebsites).map((rootDir) => {
		return stopWebsite(rootDir).then((stopped) => {
			if (stopped) {
				delete runningWebsites[rootDir];
			}
		});
	});
	await Promise.all(stopPromises);
}

app.whenReady().then(async () => {
	console.log('Creating a tray icon');
	const icon = path.join(app.getAppPath(), 'build', 'nanoweb-tray-icon@4x.png');
	console.log(icon);
	tray = await createTrayIcon(icon, (item) => {
		console.log('Selecting a website', item);
		selectWebsite(item);
	});
});

app.on('ready', async () => {
	try {
		await checkRequirements();
	} catch (err) {
		console.error(err);
		app.quit();
	}
	try {
		port = await getPort({ port: portNumbers(3006, 3333) });
		// @todo no need to generate an ssl
		// await checkAndGenerateSSLKeys(sslKey, sslCert);
		await installAddons();
		const res = createServer(port);
		server = res.server;
		serverUrl = res.url;
		await createMainWindow();
	} catch (err) {
		console.error(err);
		app.quit();
	}
});

app.on('window-all-closed', async () => {
	console.log('All windows closed. Stopping running websites:', runningWebsites);
	await stopAllWebsites();
	app.quit();
});

app.on('activate', async () => {
	if (BrowserWindow.getAllWindows().length === 0) {
		createMainWindow();
	}
});

ipcMain.on('config:save', async (event, config) => {
	saveState(config);
	updateTrayMenu();
});

ipcMain.handle('config:get', async () => { return await getState(); });

let lastClickTime = 0;
const CLICK_DELAY = 500; // 500 milliseconds
ipcMain.on('open-external', (event, url) => {
	const currentTime = new Date().getTime();
	if (currentTime - lastClickTime < CLICK_DELAY) {
		return;
	}
	lastClickTime = currentTime;
	shell.openExternal(url);
});

ipcMain.handle('open-cert-file-location', async () => {
	let command;
	switch (process.platform) {
		case 'win32':
			command = `explorer /select,${sslCert}`;
			break;
		case 'darwin':
			command = `open -R ${sslCert}`;
			break;
		case 'linux':
			command = `xdg-open ${sslCert}`;
			break;
		default:
			throw new Error('Unsupported platform');
	}
	return new Promise((resolve, reject) => {
		exec(command, (err) => {
			if (err) {
				return reject(err);
			}
			resolve();
		});
	});
});

ipcMain.handle('select-directory', async (event, currentDir) => {
	const opts = {
		properties: ['openDirectory', 'createDirectory'],
	};
	if (currentDir) {
		opts.defaultPath = currentDir;
	}
	const result = await dialog.showOpenDialog(opts);
	if (result.canceled) {
		return null;
	}
	return result.filePaths[0];
});

ipcMain.handle('get-current-directory', () => process.cwd());

(() => { // git functions
	ipcMain.on('git-init', async (event, website, dir) => {
		console.log('gitInit', website, dir);
		const git = simpleGit(website.ROOT_DIR);
		try {
			await git.init();
			event.sender.send('git-init-success', 'Git repository initialized successfully.');
		} catch (error) {
			console.error('Failed to initialize git repository:', error);
			event.sender.send('git-init-error', 'Failed to initialize git repository.');
		}
	});

	ipcMain.on('git:read', async (event, { website, dir, index }) => {
		console.log('git:read');
		const git = simpleGit(website.ROOT_DIR);
		try {
			const status = await git.status();
			const changes = {};
			changes.not_added = status.not_added;
			changes.conflicted = status.conflicted;
			changes.created = status.created;
			changes.deleted = status.deleted;
			changes.modified = status.modified;
			changes.renamed = status.renamed;
			changes.staged = status.staged;
			event.sender.send('git:read:on:success', { changes, website, dir, index });
		} catch (error) {
			console.error('Error reading git status:', error);
			event.sender.send('git:read:on:fail', error);
		}
	});

	ipcMain.on('oauth-login', async (event, provider) => {
		const authUrl = provider === 'github' ? getGithubUrl({ serverUrl }) : getGitlabUrl({ serverUrl });
		const win = createAuthWindow(event, authUrl);
	});
	
	ipcMain.on('oauth-code', async (event, data) => {
		try {
			const res = await accessToken(data.provider, data.code);
			const parsed = JSON.parse(res);
			const store = getStore();
			store[`${data.provider}Token`] = parsed.access_token;
			saveStore(store);
			if (mainWin) {
				mainWin.webContents.send('repos-list', parsed);
			}
		} catch (error) {
			console.error(error);
		}
	});
	
	ipcMain.on('git-connect2github', async (event, website, dir) => {
		shell.openExternal(`http://localhost:${port}/auth/github`);
	});
	
	ipcMain.on('git-connect2gitlab', async (event, website, dir) => {
		shell.openExternal(`http://localhost:${port}/auth/gitlab`);
	});
	
	ipcMain.on('git-save-url', async (event, website, dir, url) => {
		console.log('gitSaveUrl', website, dir, url);
		// Implement the functionality to save the URL to the Git configuration or wherever it's needed
		// Example:
		try {
			// Perform save operation
			event.sender.send('git-save-url-success', 'Git URL saved successfully.');
		} catch (error) {
			console.error('Failed to save Git URL:', error);
			event.sender.send('git-save-url-error', 'Failed to save Git URL.');
		}
	});
})();

(() => { // website functions
	ipcMain.handle('website:start', async (event, { website }) => {
		const result = await selectWebsite(website);
		return result;
	});
	
	ipcMain.handle('website:stop', async (event, rootDir) => {
		return await stopWebsite(rootDir);
	});
	
	ipcMain.handle('website:show', async (event, website) => {
		return await showWebsite(website);
	});
	
	ipcMain.handle('website:hide', async (event) => {
		return await hideWebsite();
	});

	ipcMain.handle('website:remove', async (event, { website, dir, index }) => {
		try {
			const res = await removeWebsite(website);
			return res;
		} catch (error) {
			console.error('website:remove', error);
		}
		return false;
	});
})();

ipcMain.handle('quit', () => app.quit());
