import { app } from 'electron';
import path from 'node:path';
import fs from 'node:fs';
import { exec } from 'child_process';
import crypto from 'crypto';
import { savePackageConfig, install } from './server.js';
import { loadYAML, saveYAML, ensureDirectory, extension, copy, loadJSON } from './fs.js';
import config from '../config.js';

const { DIST_DIR, DIST_URI } = config;

function checkApp(app) {
	return new Promise((resolve, reject) => {
		const command = process.platform === 'win32' ? `where ${app}` : `which ${app}`;
		exec(command, (error, stdout, stderr) => {
			if (error) {
				reject(`Required application not found: ${app}. Please install it.`);
			} else {
				console.log(`${app} is installed: ${stdout.trim()}`);
				resolve();
			}
		});
	});
}

export function getUserFile(file) {
	return path.join(app.getPath('userData'), file);
}

export function getBuildFile(file) {
	return path.join(app.getAppPath(), DIST_URI, file);
}

export async function checkRequirements() {
	const requiredApps = [
		// 'openssl',
	];
	for (const app of requiredApps) {
		await checkApp(app);
	}
}

export async function checkAndGenerateSSLKeys(sslKey, sslCert) {
	const sslKeyFile = getUserFile(sslKey);
	const sslCertFile = getUserFile(sslCert);
	if (!fs.existsSync(sslKeyFile) || !fs.existsSync(sslCertFile)) {
		let keyDir = path.dirname(sslKeyFile);
		if (!fs.existsSync(keyDir)) fs.mkdirSync(keyDir, { recursive: true });
		keyDir = path.dirname(sslCertFile);
		if (!fs.existsSync(keyDir)) fs.mkdirSync(keyDir, { recursive: true });
		console.log('SSL keys not found, generating...');
		const opensslCommand = `
			openssl req -nodes -new -x509 -keyout "${sslKeyFile}" -out "${sslCertFile}" -days 365 \
			-subj "/C=UA/ST=ZP/L=Zaporizhia/O=yaro.page/OU=law/CN=localhost" \
			-addext "subjectAltName=DNS:localhost" \
			-addext "keyUsage=digitalSignature,keyEncipherment" \
			-addext "extendedKeyUsage=serverAuth" \
			-addext "basicConstraints=CA:FALSE"
			`.replace(/\s+/g, ' ');

		return new Promise((resolve, reject) => {
			exec(opensslCommand, (error, stdout, stderr) => {
				if (error) {
					console.error(`Error generating SSL keys: ${error.message}`);
					return reject(error);
				}
				console.log(`SSL keys generated successfully:\n${stdout}`);
				if (stderr) {
					console.error(`OpenSSL stderr: ${stderr}`);
				}
				resolve();
			});
		});
	} else {
		console.log('SSL keys already exist.');
		return Promise.resolve();
	}
}

function setupGit(rootDir) {
	const res = { git: false, url: null };
	// Check for .git/config
	const gitConfigPath = path.join(rootDir, '.git', 'config');
	if (fs.existsSync(gitConfigPath)) {
		res.git = true;
		const gitConfigContent = fs.readFileSync(gitConfigPath, 'utf8');
		const remoteOriginUrl = gitConfigContent.match(/url\s*=\s*(.*)/);
		if (remoteOriginUrl) {
			res.url = remoteOriginUrl[1].replace(/:\/\/[^@]*@/, '://');
		}
	}
	return res;
}

export async function setupPackage(website) {
	const packagePath = path.join(website.ROOT_DIR, 'package.json');
	let packageConfig = {};
	if (fs.existsSync(packagePath)) {
		packageConfig = loadJSON(packagePath);
	} else {
		packageConfig = savePackageConfig(website, packagePath);
	}
	const lockPath = path.join(website.ROOT_DIR, 'package-lock.json');
	if (!fs.existsSync(lockPath)) {
		await install(website.ROOT_DIR);
	}
	return packageConfig;
}

async function getConfigIcon(website) {
	const res = { nanoweb: false, icon: false, runtime: {} };
	const nanowebConfigPath = path.join(website.ROOT_DIR, 'nanoweb.config.js');
	if (!fs.existsSync(nanowebConfigPath)) {
		return res;
	}
	try {
		const cfg = await import(nanowebConfigPath);
		const runtime = cfg.default.private();
		if (runtime) {
			res.nanoweb = true;
			res.runtime = runtime;
			const dataFile = path.join(runtime?.['DATA_DIR'], '_.yaml');
			const data = loadYAML(dataFile);
			res.icon = data?.['$icon'] || false;
		}	
	} catch (error) {
		console.error('Unable to load nanoweb.config.js', error);
	}
	return res;
}

export function getConfigIconPath(target) {
	return getBuildFile(getConfigIconUri(target).replace(/^\/+/, ''));
}

function getConfigIconUri(target) {
	const { website, packageConfig, src } = target;
	const name = packageConfig?.['name'] || website.HOST.replace(/[^\w]+/g, '-');
	const ext = extension(src, '.');
	return `/websites/${name}${ext}`;
}

function copyConfigIcon(target, insistCopy = false) {
	const { src } = target;
	const distIconPath = getConfigIconPath(target);
	if (!fs.existsSync(distIconPath) || insistCopy) {
		ensureDirectory(path.dirname(distIconPath));
		copy(src, distIconPath);
	}
	return getConfigIconUri(target);
}

async function setupConfig(website, packageConfig) {
	const res = { nanoweb: false, icon: false, runtime: {} };
	const icon = await getConfigIcon(website);
	const runtime = icon.runtime;
	res.nanoweb = icon.nanoweb;
	if (icon.icon) {
		const src = path.join(runtime?.['STATIC_DIR'], '.', icon.icon);
		const target = { website, src, packageConfig };
		res.icon = copyConfigIcon(target);
	}
	return res;
}

export async function removeWebsite(website) {
	const pkg = await setupPackage(website);
	const iconPath = getConfigIconPath(website, pkg);
	if (fs.existsSync(iconPath)) {
		fs.unlink(iconPath);
	}
}

export async function loadState(file) {
	if (!fs.existsSync(file)) {
		console.error('State file missing', file);
		return false;
	}
	try {
		const state = loadYAML(file);
		state.dirs = [];
		for (const site of (state.websites || [])) {
			const dirInfo = { git: false, url: null, nanoweb: false, port: null, icon: false };
			if (fs.existsSync(site.ROOT_DIR)) {
				const git = setupGit(site.ROOT_DIR);
				dirInfo.git = git.git;
				dirInfo.url = git.url;
				// Create package.json and install npm packages
				const packageConfig = await setupPackage(site);
				// Check for nanoweb.config.js
				const res = await setupConfig(site, packageConfig);
				dirInfo.nanoweb = res.nanoweb;
				dirInfo.icon = res.icon;
				dirInfo.port = res?.runtime?.SERVER_PORT || null;
			}
			state.dirs.push(dirInfo);
		}
		return state;
	} catch (error) {
		console.error(`Error loading state from file ${file}:`, error);
		return null;
	}
}

function getStateFile() {
	return getUserFile('state.yaml');
}

export async function getState() {
	const stateFile = getStateFile();
	const defaultState = {
		activeWebsite: -1,
		websites: [],
		windowState: {
			width: 800,
			height: 600,
			x: undefined,
			y: undefined,
			isFullScreen: false
		}
	};
	const loaded = await loadState(stateFile);
	return loaded || defaultState;
}

export function saveState(state) {
	const file = getStateFile();
	try {
		saveYAML(file, state);
	} catch (error) {
		console.error(`Error saving state to file ${file}:`, error);
	}
}

const algorithm = 'aes-256-ctr';
const iv = crypto.randomBytes(16);

function encrypt(text, pin) {
	const cipher = crypto.createCipheriv(algorithm, crypto.scryptSync(pin, 'salt', 32), iv);
	const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
	return `${iv.toString('hex')}:${encrypted.toString('hex')}`;
}

function decrypt(encrypted, pin) {
	const [ivHex, encryptedText] = encrypted.split(':');
	const decipher = crypto.createDecipheriv(algorithm, crypto.scryptSync(pin, 'salt', 32), Buffer.from(ivHex, 'hex'));
	const decrypted = Buffer.concat([decipher.update(Buffer.from(encryptedText, 'hex')), decipher.final()]);
	return decrypted.toString();
}

function getStoreFile() {
	return getUserFile('store.yaml');
}
export function decryptStore(store, pin) {
	const decryptedStore = {};
	Object.keys(store).forEach(key => {
		decryptedStore[key] = decrypt(store[key], pin);
	});
	return decryptedStore;
}
export function encryptStore(store, pin) {
	const encryptedStore = {};
	Object.keys(store).forEach(key => {
		encryptedStore[key] = encrypt(store[key], pin);
	});
	return encryptedStore;
}
function loadStore(file, pin) {
	if (!fs.existsSync(file)) {
		console.error('Store file missing', file);
		return false;
	}
	try {
		const store = loadYAML(file);
		return decryptStore(store, pin);
	} catch (error) {
		console.error(`Error loading state from file ${file}:`, error);
		return null;
	}
}
export function getStore(pin) {
	const file = getStoreFile();
	const defaultStore = {
		'githubToken': null,
		'gitlabToken': null,
	};
	return loadStore(file, pin) || defaultStore;
}
export function saveStore(store, pin) {
	const file = getStoreFile();
	const encryptedStore = encryptStore(store, pin);
	saveYAML(file, encryptedStore);
}