import fetch from 'node-fetch';
import config from '../config.js';

const { github, gitlab } = config;

const getLocalUri = (req, affix = '') => {
	const protocol = req.protocol === 'https' ? 'https' : 'http';
	const host = req.headers.host; // This will include the port if it's not the default port (80 for HTTP, 443 for HTTPS)
	return `${protocol}://${host}/auth/callback${affix}`;
};

export function getGithubUrl({ serverUrl, redirectUri }) {
	if ('undefined' === typeof redirectUri) {
		redirectUri = `${serverUrl}/auth/callback/github`;
	}
	return `${github.authUrl}?client_id=${github.clientId}&redirectUri=${encodeURIComponent(redirectUri)}&scope=repo&state=github`;
};

export async function githubAuth(req, res) {
	const authUrl = getGithubUrl({ redirectUri: getLocalUri(req, '/github') });
	res.writeHead(302, { Location: authUrl });
	res.end();
}

export function getGitlabUrl({ serverUrl, redirectUri }) {
	if ('undefined' === typeof redirectUri) {
		redirectUri = `${serverUrl}/auth/callback/github`;
	}
	return `${gitlab.authUrl}?client_id=${gitlab.clientId}&redirectUri=${encodeURIComponent(redirectUri)}&response_type=code&scope=read_user&state=gitlab`;
};

export async function gitlabAuth(req, res) {
	const authUrl = getGitlabUrl({ redirectUri: getLocalUri(req, '/gitlab') });
	res.writeHead(302, { Location: authUrl });
	res.end();
}

export async function accessToken(provider, code) {
	const url = `https://nanoweb.yaro.page/auth.php?state=${provider}&code=${code}`;
	try {
		const response = await fetch(url);
		const repositories = await response.json();
		return repositories;
	} catch (error) {
		throw new Error(error);
	}
}

export async function handleCallback(req, res) {
	const urlParams = new URLSearchParams(req.url.split('?')[1]);
	const code = urlParams.get('code');

	try {
		const response = await fetch(`https://nanoweb.yaro.page/auth.php?code=${code}`);
		const repositories = await response.json();
		res.writeHead(200, { 'Content-Type': 'application/json' });
		res.end(JSON.stringify(repositories));
	} catch (error) {
		res.writeHead(500, { 'Content-Type': 'application/json' });
		res.end(JSON.stringify({ error: error.message }));
	}
}
