import fs from 'node:fs';
import path from 'node:path';
import yaml from 'yaml';

export function removeDirectory(directoryPath, recursively = false) {
	if (fs.existsSync(directoryPath)) {
		fs.readdirSync(directoryPath).forEach((file) => {
			const currentPath = path.join(directoryPath, file);
			if (fs.lstatSync(currentPath).isDirectory()) {
				// Recursively delete directory
				if (recursively) removeDirectory(currentPath, true);
			} else {
				// Delete file
				fs.unlinkSync(currentPath);
			}
		});
		fs.rmdirSync(directoryPath);
	}
}

export function ensureDirectory(dir, removeBefore = false) {
	if (removeBefore && fs.existsSync(dir)) {
		removeDirectory(dir, true);
	}
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir, { recursive: true });
	}
}

export function loadYAML(file) {
	const content = fs.readFileSync(file, { encoding: 'utf-8' });
	try {
		const data = yaml.parse(content);
		return data;
	} catch (err) {
		console.error('Cannot parse file', file, err);
		return null;
	}
}

export function saveYAML(file, data) {
	return fs.writeFileSync(file, yaml.stringify(data));
}

export function loadJSON(file) {
	const content = fs.readFileSync(file);
	return JSON.parse(content);
}

export function saveJSON(file, data) {
	return fs.writeFileSync(file, JSON.stringify(data));
}

export function copy(src, dest, recursive = false) {
	const stat = fs.lstatSync(src);

	// Check if source is a directory
	if (stat.isDirectory()) {
		if (!recursive) {
			throw new Error('Source is a directory, but recursive flag is not set.');
		}

		// Ensure the destination directory exists
		ensureDirectory(dest);

		// Copy each item in the directory
		const items = fs.readdirSync(src);
		for (const item of items) {
			const srcPath = path.join(src, item);
			const destPath = path.join(dest, item);
			copy(srcPath, destPath, true);
		}
	} else {
		// Copy the file
		fs.copyFileSync(src, dest);
	}
}

export function remove(dest, recursive = false) {
	if (fs.existsSync(dest)) {
		const stat = fs.lstatSync(dest);

		if (stat.isDirectory()) {
			if (!recursive) {
				throw new Error('Cannot remove directory without recursive flag set.');
			}

			// Remove directory recursively
			fs.readdirSync(dest).forEach((file) => {
				const currentPath = path.join(dest, file);
				if (fs.lstatSync(currentPath).isDirectory()) {
					remove(currentPath, true);
				} else {
					fs.unlinkSync(currentPath);
				}
			});
			fs.rmdirSync(dest);
		} else {
			// Remove file
			fs.unlinkSync(dest);
		}
	}
}

export function extension(file, prefix = '') {
	// file.slice(((filePath.lastIndexOf(".") - 1) >>> 0) + 2)
	const parts = file.split('.');
	let ext = parts.length > 1 ? parts[parts.length - 1] : '';
	if (ext.length > 5) ext = '';
	if ('html' === ext && parts.length > 2 && parts[parts.length - 2] === 'embed') {
		ext = parts.slice(parts.length - 2).join('.');
	}
	return ext === '' ? '' : (prefix + ext);
}
