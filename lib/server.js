import { BrowserWindow } from 'electron';
import fs from 'node:fs';
import http from 'node:http';
import https from 'node:https';
import path from 'node:path';
import { exec } from 'child_process';
import treeKill from 'tree-kill';
import config from '../config.js';
import { githubAuth, gitlabAuth, handleCallback } from './auth.js';
import { getUserFile, getBuildFile } from './app.js';

// Destructure config and provide default value for httpsPort
const { sslKey, sslCert, sslEnabled, DIST_DIR } = config;

// Function to serve static files
function serveStaticFile(filePath, res) {
	fs.readFile(filePath, (err, data) => {
		if (err) {
			res.writeHead(404, { 'Content-Type': 'text/plain' });
			res.end('Not Found: ' + err.message);
			return;
		}

		const ext = path.extname(filePath).toLowerCase();
		const mimeTypes = {
			'.html': 'text/html',
			'.js': 'application/javascript',
			'.css': 'text/css',
			'.png': 'image/png',
			'.jpg': 'image/jpeg',
			'.gif': 'image/gif',
			'.svg': 'image/svg+xml',
			'.json': 'application/json'
		};

		const contentType = mimeTypes[ext] || 'application/octet-stream';
		res.writeHead(200, { 'Content-Type': contentType });
		res.end(data);
	});
}

// Request handler
const requestHandler = (req, res) => {
	res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate'); // HTTP 1.1
	res.setHeader('Pragma', 'no-cache'); // HTTP 1.0
	res.setHeader('Expires', '0'); // Proxies
	res.setHeader('Content-Type', 'application/json');

	if (req.url.startsWith('/auth/github')) {
		return githubAuth(req, res);
	} else if (req.url.startsWith('/auth/gitlab')) {
		return gitlabAuth(req, res);
	} else if (req.url.startsWith('/auth/callback')) {
		return handleCallback(req, res);
	} else if (req.url === '/' || req.url === '/index.html') {
		const filePath = getBuildFile('index.html');
		return serveStaticFile(filePath, res);
	} else {
		const filePath = getBuildFile(req.url);
		if (fs.existsSync(filePath)) {
			return serveStaticFile(filePath, res);
		} else {
			res.writeHead(404, { 'Content-Type': 'text/plain' });
			res.end('Not Found: ' + req.url + "\n" + filePath);
		}
	}
};

export function createServer(port = 3000) {
	const provider = sslEnabled ? https : http;
	const serverOptions = sslEnabled
		? {
			key: fs.readFileSync(getUserFile(sslKey)),
			cert: fs.readFileSync(getUserFile(sslCert))
		}
		: undefined;

	const server = provider.createServer(serverOptions, requestHandler);
	server.listen(port, () => {
		console.log(`Server running on port ${port}`);
	});
	return { server, url: `http${sslEnabled ? 's' : ''}://localhost:${port}` };
}

const serverProcesses = {};

export function savePackageConfig(website, filePath) {
	const url = new URL(website.HOST);
	const name = url.hostname;
	const defaultConfig = {
		"name": name,
		"version": "1.0.0",
		"description": "Static site generator on Javascript.",
		"main": "server.js",
		"dependencies": {
			"nanoweb": "file:~/Projects/npm/nanoweb"
		},
		// "repository": {
		// 	"type": "git",
		// 	"url": "git+https://github.com/yaro-rasta/nanoweb.git"
		// },
		"keywords": [
			"ssg",
			"static",
			"site",
			"static",
			"website",
			"generator"
		],
		"author": "Yaro",
		"license": "MIT",
		// "bugs": {
		// 	"url": "https://github.com/yaro-rasta/nanoweb/issues"
		// },
		"homepage": "https://nanoweb.yaro.page"
	};
	fs.writeFileSync(filePath, JSON.stringify(defaultConfig, null, 2));
}

export function install(cwd) {
	return new Promise((resolve, reject) => {
		// force overwrite files for the local /npm/nanoweb
		const installCommand = 'npm install --force';
		exec(installCommand, { cwd }, (installError, installStdout, installStderr) => {
			if (installError) {
				console.error(`Error running npm install: ${installError}`);
				return reject(installError);
			}
			console.log(`npm install completed in directory: ${cwd}`);
			console.log(`stdout: ${('' + installStdout).trim()}`);
			if (installStderr) {
				console.log(`stderr: ${('' + installStderr).trim()}`);
			}
			resolve();
		});
	});
}

export function start(website, { onLog }) {
	return new Promise((resolve, reject) => {
		const cwd = website.ROOT_DIR;
		const startCommand = path.join(cwd, 'node_modules', '.bin', 'nanoweb') + ' server';
		const serverProcess = exec(startCommand, { cwd });
	
		serverProcesses[cwd] = serverProcess;
	
		serverProcess.stdout.on('data', (data) => {
			console.log(`stdout: ${('' + data).trim()}`);
			if ('function' === typeof onLog) onLog({ type: 'stdout', data, website });
			BrowserWindow.getAllWindows().forEach(win => {
				win.webContents.send('website:start:on:log', { type: 'stdout', data, website });
			});
		});
	
		serverProcess.stderr.on('data', (data) => {
			console.error(`stderr: ${('' + data).trim()}`);
			if ('function' === typeof onLog) onLog({ type: 'stderr', data, website });
			BrowserWindow.getAllWindows().forEach(win => {
				win.webContents.send('website:start:on:log', { type: 'stderr', data, website });
			});
		});
	
		serverProcess.on('error', (error) => {
			console.error(`Error starting server: ${('' + error).trim()}`);
			if ('function' === typeof onLog) onLog({ type: 'stderr', error, website });
			BrowserWindow.getAllWindows().forEach(win => {
				win.webContents.send('website:start:on:fail', error.message, website);
			});
			return reject(error);
		});
	
		serverProcess.on('close', (code) => {
			const message = `Server process exited with the code ${code}`;
			console.log(message);
			delete serverProcesses[cwd];
			if (code) {
				reject(message);
			} else {
				resolve();
			}
		});
	});
}

export function startServerWithConfig(website, { onLog }) {
	return new Promise((resolve, reject) => {
		if (!website.ROOT_DIR) {
			return reject('ROOT_DIR is undefined');
		}
		install(website.ROOT_DIR).then(() => {
			// const startCommand = 'nanoweb server';
			// Then start the server using the local nanoweb binary
			start(website, { onLog }).then(() => {
				resolve();
			}, (error) => {
				reject(error);
			});
		}, (error) => {
			reject(error);
		});
	});
}

export function stopWebsite(rootDir) {
	return new Promise((resolve, reject) => {
		const serverProcess = serverProcesses[rootDir];
		if (!serverProcess) {
			return resolve(false); // Server is not running
		}

		serverProcess.on('close', () => {
			console.log(`Server at ${rootDir} has been stopped`);
			resolve(true);
		});

		serverProcess.on('error', (error) => {
			console.error(`Error stopping server at ${rootDir}: ${error}`);
			reject(error);
		});

		treeKill(serverProcess.pid, 'SIGTERM', (err) => {
			if (err) {
				console.error(`Failed to kill process ${serverProcess.pid}: ${err}`);
				reject(err);
			} else {
				console.log(`Sent SIGTERM to process ${serverProcess.pid}`);
			}
		});
	});
}