import { app, BrowserWindow, BaseWindow, WebContentsView, Menu, Tray, ipcMain, screen } from 'electron';
import path from 'node:path';
import fs from 'node:fs';
import config from '../config.js';
import contextMenu from 'electron-context-menu';
import { getUserFile, getState, saveState } from './app.js';
const { DIST_URI, DIST_DIR, sslEnabled } = config;

import log from 'electron-log';
log.initialize();
log.transports.file.resolvePathFn = () => getUserFile('app.log');
console.log = log.log;
Object.assign(console, log.functions);

const isDev = process.env.NODE_ENV === 'development';
const navWidth = 60;

export async function createWindow(serverUrl) {
	const { windowState } = await getState();
	const { width, height, x, y, isFullScreen } = windowState || { width: 800, height: 600, x: undefined, y: undefined, isFullScreen: false };
	const webPreferences = {
		preload: path.join(app.getAppPath(), 'preload.js'),
		contextIsolation: true,
		enableRemoteModule: false,
		nodeIntegration: false
	};

	let win = new BrowserWindow({
		width,
		height,
		x,
		y,
		webPreferences,
		icon: path.join(app.getAppPath(), 'build', 'icons', '1024x1024.png')
	});
	if (isFullScreen) {
		win.setFullScreen(true);
	}

	const [contentWidth, contentHeight] = win.getContentSize();
	const mainView = new WebContentsView({ webPreferences });
	mainView.setBounds({ x: 0, y: 0, width: contentWidth, height: contentHeight });
	win.contentView.addChildView(mainView);

	const websiteView = new WebContentsView({ webPreferences });
	websiteView.setBounds({ x: navWidth, y: 0, width: 0, height: contentHeight });
	win.contentView.addChildView(websiteView);

	if (isDev) {
		contextMenu({
			window: mainView,
			showInspectElement: true,
		});
		contextMenu({
			window: websiteView,
			showInspectElement: true,
		});
	}

	const webContents = mainView.webContents;

	webContents.on('did-finish-load', () => {
		if (isDev) {
			webContents.openDevTools();
		}
		webContents.setZoomFactor(1);
		webContents.setVisualZoomLevelLimits(1, 1);
		console.log('Window loaded', isDev ? 'development' : 'production');
	});

	webContents.on('crashed', () => {
		console.log('Window crashed');
	});

	webContents.on('unresponsive', () => {
		console.log('Window became unresponsive');
	});

	webContents.on('devtools-opened', () => {
		console.log('DevTools opened');
	});

	let failedToLoad = false;
	webContents.on('did-fail-load', (event, errorCode, errorDescription, validatedURL) => {
		console.error(errorCode, errorDescription);
		// ERR_CERT_AUTHORITY_INVALID
		// ERR_CERT_COMMON_NAME_INVALID
		if (failedToLoad || !errorDescription.includes('ERR_CERT_AUTHORITY_INVALID')) {
			return;
		}
		const locale = app.getLocale().split('-')[0];
		const localeUri = `guide/certs/${locale}.html`;
		const fallbackUri = 'guide/certs/uk.html';
		const localeFilePath = path.join(DIST_DIR, localeUri);
		const uri = fs.existsSync(localeFilePath) ? localeUri : fallbackUri;
		failedToLoad = true;
		win.loadFile(`${DIST_URI}/${uri}`);
	});

	win.on('close', async () => {
		const bounds = win.getBounds();
		const state = await getState();
		state.windowState = {
			width: bounds.width,
			height: bounds.height,
			x: bounds.x,
			y: bounds.y,
			isFullScreen: win.isFullScreen()
		};
		saveState(state);
	});

	win.on('closed', () => {
		win = null;
	});
	win.on('resize', () => {
		const [newWidth, newHeight] = win.getContentSize();
		let { width: mainWidth } = mainView.getBounds();
		let { width: websiteWidth } = websiteView.getBounds();
		if (websiteWidth > 0) {
			websiteWidth = newWidth - mainWidth;
		} else {
			mainWidth = newWidth;
		}
		mainView.setBounds({ x: 0, y: 0, width: mainWidth, height: newHeight });
		websiteView.setBounds({ x: navWidth, y: 0, width: websiteWidth, height: newHeight });
	});
	mainView.webContents.loadURL(serverUrl);

	return { win, mainView, websiteView };
}

export async function installAddons() {
	// Install React DevTools if in development mode
	if (process.env.NODE_ENV === 'development') {
		// Dynamically import the extension installer
		const { default: installExtension, REACT_DEVELOPER_TOOLS } = await import('electron-devtools-installer');
		try {
			await installExtension.default(REACT_DEVELOPER_TOOLS);
			console.log('React DevTools installed.');
		} catch (err) {
			console.log('Error: React DevTools.', err);
		}
	}
}

let tray;
let onTrayMenuClick;
async function createContextMenu() {
	const state = await getState();
	const items = (state?.websites || []).map((website, index) => ({
		label: website.HOST,
		type: 'radio',
		checked: index === state.activeWebsite,
		click: () => {
			onTrayMenuClick(website);
			updateTrayMenu();
		}
	}));

	items.push({ type: 'separator' }); // Add a divider

	items.push({
		label: 'Quit',
		type: 'normal',
		click: () => {
			app.quit();
		}
	});

	return Menu.buildFromTemplate(items);
}

export async function updateTrayMenu() {
	if (!tray) return;
	const menu = await createContextMenu();
	tray.setContextMenu(menu);
}

export async function createTrayIcon(icon, onClick) {
	tray = new Tray(icon);
	onTrayMenuClick = onClick;
	tray.setToolTip('This is my application.');
	await updateTrayMenu();
	return tray;
}

export async function createAuthWindow(event, authUrl) {
	const { width: screenWidth, height: screenHeight } = screen.getPrimaryDisplay().workAreaSize;
	const width = Math.min(630, screenWidth);
	const height = Math.min(840, screenHeight);
	const authWindow = new BrowserWindow({
		width,
		height,
		show: false,
		webPreferences: {
			nodeIntegration: false
		}
	});

	authWindow.loadURL(authUrl);
	authWindow.show();

	authWindow.webContents.on('will-redirect', (event, url) => {
		const codeMatch = url.match(/code=([^&]*)/);
		const providerMatch = url.match(/state=([^&]*)/); // Extract provider from the URL
		if (codeMatch && providerMatch) {
			event.preventDefault();
			authWindow.close();
			ipcMain.emit('oauth-code', event, { provider: providerMatch[1], code: codeMatch[1] });
		}
	});

	authWindow.on('closed', () => {
		authWindow = null;
	});
	return authWindow;
}

export async function createWebsiteWindow(parentWin, url = null) {
	const win = new BrowserWindow({
		width: 800,
		height: 600,
		parent: parentWin,
		webPreferences: {
			nodeIntegration: false,
			contextIsolation: true,
		}
	});

	if (url) win.loadURL(url);
	win.on('closed', () => {
		win = null;
	});

	return win;
}

export async function adjustWindowSize(target, { win, mainView, websiteView }) {
	const [width, height] = win.getContentSize();
	if (isDev) {
		if (target == 'website') {
			if (mainView.webContents.isDevToolsOpened()) {
				mainView.webContents.closeDevTools();
			}
			if (!websiteView.webContents.isDevToolsOpened()) {
				websiteView.webContents.openDevTools();
			}
		}
	}
	if (target === 'website') {
		mainView.setBounds({ x: 0, y: 0, width: navWidth, height });
		websiteView.setBounds({ x: navWidth, y: 0, width: width - navWidth, height });
	} else {
		mainView.setBounds({ x: 0, y: 0, width, height });
		websiteView.setBounds({ x: width, y: 0, width: 0, height });
	}
}
