import path from 'node:path';
import fs from 'fs-extra';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import WebpackShellPluginNext from 'webpack-shell-plugin-next';
import yaml from 'yaml';
import appConfig from './config.js';

const { DIST_DIR, PUBLIC_DIR, PUBLIC_URI } = appConfig;

class CleanAndConvertYamlPlugin {
	apply(compiler) {
		compiler.hooks.emit.tapAsync('CleanAndConvertYamlPlugin', (compilation, callback) => {
			fs.emptyDirSync(DIST_DIR);

			// Convert YAML files to JSON recursively
			function convertYamlToJson(dir) {
				fs.readdirSync(dir).forEach(file => {
					const fullPath = path.join(dir, file);
					const stat = fs.statSync(fullPath);

					if (stat.isDirectory()) {
						convertYamlToJson(fullPath);
					} else {
						const ext = path.extname(file).toLowerCase();
						if (ext === '.yaml' || ext === '.yml') {
							const jsonPath = path.join(DIST_DIR, path.relative(PUBLIC_DIR, fullPath).replace(ext, '.json'));
							const yamlContent = fs.readFileSync(fullPath, 'utf8');
							const jsonContent = yaml.parse(yamlContent);
							fs.ensureDirSync(path.dirname(jsonPath));
							fs.writeFileSync(jsonPath, JSON.stringify(jsonContent, null, 2));
						}
					}
				});
			}

			convertYamlToJson(PUBLIC_DIR);

			callback();
		});
	}
}

const config = {
	mode: 'development',
	entry: './src/index.js',
	output: {
		path: DIST_DIR,
		filename: 'main.js'
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env', '@babel/preset-react'],
					},
				},
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				],
			},
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader',
				],
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
				type: 'asset/resource',
				generator: {
					filename: 'fonts/[name][ext]',
				},
			}
		]
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: `./${PUBLIC_URI}/index.html`
		}),
		new CopyWebpackPlugin({
			patterns: [
				{ from: PUBLIC_URI, to: '.', globOptions: { ignore: ['**/index.html', '**/languages/*.yaml'] } }
			]
		}),
		new CleanAndConvertYamlPlugin(),
	],
	devServer: {
		static: {
			directory: DIST_DIR,
		},
		compress: true,
		port: 9000,
		watchFiles: [
			`${PUBLIC_URI}/**/*`,
			'src/**/*'
		],
	}
};

const isDev = process.env.NODE_ENV === 'development';
if (!isDev) {
	config.plugins.push(new WebpackShellPluginNext({
		onBuildStart: {
			scripts: ['echo "Starting Webpack build process..."'],
			blocking: true,
			parallel: false,
		},
		onBuildEnd: {
			scripts: ['node build-icons.js'],
			blocking: true,
			parallel: false,
		},
	}));
}

export default config;
